<body>
    <form action="sort" method="POST">
        <div class="row m-2">
            <?php 
                function selected($var, $value) 
                {
                    if (!is_array($var)) {
                        $var = explode(',', $var);
                    }
                
                    return (in_array($value, $var)) ? ' selected' : '';
                }
            ?>
            <div class="col-md-6">
                <label for="categorySelector" class="form-label">Категория</label>
                <select name="category" id="categorySelector" class="form-select" aria-label="Default select example">
                    <option <?php echo selected($category, 0)?> value="0">Адрес электронной почты</option>
                    <option <?php echo selected($category, 1)?> value="1">Имя пользователя</option>
                    <option <?php echo selected($category, 2)?> value="2">Статус</option>
                </select>
            </div>
            <div class="col-md-6">
                <label for="order" class="form-label">Категория</label>
                <select name="order" id="order" class="form-select" aria-label="Default select example">
                    <option <?php echo selected($order, 0)?> value="0">Возрастание</option>
                    <option <?php echo selected($order, 1)?> value="1">Убывание</option>
                </select>
            </div>
        </div>
        <button type="Submit" class="btn btn-success m-3">
            Сортировать!
        </button>
    </form>
    <div class="row">
        <?php foreach ($data as $task) {?>
        <div class="col-md-4">
            <div class='card m-3 <?php if ($task['complited']) echo "bg-success"?>'>
                <div class='card-body'>
                    <h5 class='card-title doted'>
                        <?php echo $task['userName'] ?></h5>
                    <p class='card-text doted task-email'>
                        <?php echo $task['email'] ?></p>
                    <p class='card-text oted task-description'>
                        <?php echo $task['description'] ?> </p>
                    <p class='card-text m-1 h-20px doted text-danger'>
                        <?php if ($task['edited']) echo '(Отредактированно администратором!) ';?></p>
                    <p class='card-text m-1 h-20px doted'>
                        <?php if ($task['complited']) echo "<h5>Выполнено!</h5>"; else echo "<h5>Не выполнено</h5>"?>
                </div>
                <?php if ($isAdmin): ?>
                <form action="delete?id=<?php echo $task['id']; ?>" method="POST">
                    <button type="Submit" class="btn btn-danger m-1">
                        Удалить
                    </button>
                </form>
                <?php if (!$task['complited']): ?>
                <form action="complited?id=<?php echo $task['id']; ?>" method="POST">
                    <button type="Submit" class="btn btn-success m-1">
                        Выполнено!
                    </button>
                </form>
                <?php endif; ?>
                <button type="button" class="btn btn-warning m-1" data-bs-toggle="modal" data-bs-target="#editModal<?php echo $task['id']; ?>">
                    Изменить
                </button>
                <div class="modal fade" id="editModal<?php echo $task['id']; ?>" tabindex="-1" aria-labelledby="editModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                                <h5 class="modal-title" id="editModalLabel">Изменить</h5>
                            </div>
                            <form action="edit?id=<?php echo $task['id']; ?>" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="email">Адрес электронной почты</label>
                                        <input name="email" type="email" class="form-control" id="email"
                                            placeholder="Введите адрес электронной почты" required
                                            value="<?php echo $task['email'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="userName">Имя пользователя</label>
                                        <input required name="userName" type="text" class="form-control" id="userName"
                                            placeholder="Введите имя пользователя..."
                                            value="<?php echo $task['userName'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Описание</label>
                                        <textarea required name="description" type="text" class="form-control"
                                            id="description"
                                            placeholder="Введите описание..."><?php echo $task['description'] ?></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Закрыть</button>
                                    <button type="Submit" class="btn btn-primary">Изменить!</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php endif; ?>
            </div>
        </div>
        <?php }?>
    </div>
    <ul class="pagination ms-3">
        <li><a class="page-link" href="?pageno=1">First</a></li>
        <li class="page-item <?php if ($pageno <= 1) {echo 'disabled';}?>">
            <a class="page-link"
                href="<?php if ($pageno <= 1) {echo '#';} else {echo "?pageno=" . ($pageno - 1);}?>">Prev</a>
        </li>
        <?php for ($i = 1; $i <= $total_pages; $i++) { ?>
        <li class="page-item <?php if ($pageno == $i) {echo 'disabled';}?>">
            <a class="page-link"
                href="<?php if ($pageno == $i) {echo '#';} else {echo "?pageno=" . ($i);}?>"><?php echo "$i"; ?></a>
        </li>
        <?php }?>
        <li class="page-item <?php if ($pageno >= $total_pages) {echo 'disabled';}?>">
            <a class="page-link"
                href="<?php if ($pageno >= $total_pages) {echo '#';} else {echo "?pageno=" . ($pageno + 1);}?>">Next</a>
        </li>
        <li><a class="page-link" href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
    </ul>
    <?php if($adminLogError) echo "
        <script> $.toast({
            heading: 'Error',
            text: 'Incorrect login or password',
            icon: 'error',
            hideAfter: 5000
        }) </script>"
    ;?>
    <?php if($error) echo "
        <script> $.toast({
            heading: 'Error',
            icon: 'error',
            hideAfter: 5000
        }) </script>"
    ;?>
    <?php if($adminLogSuccess) echo "
        <script> $.toast({
            heading: 'Success',
            text: 'Welcome, Admin',
            icon: 'success',
            hideAfter: 5000
        }) </script>"
    ;?>
    <?php if($success) echo "
        <script> $.toast({
            heading: 'Success',
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: 5000
        }) </script>"
    ;?>
    <button type="button" class="btn btn-primary m-3" data-bs-toggle="modal" data-bs-target="#addModal">
        Добавить
    </button>
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h5 class="modal-title" id="addModalLabel">Добавить</h5>
                </div>
                <form action="add" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email">Адрес электронной почты</label>
                            <input name="email" type="email" class="form-control" id="email"
                                placeholder="Введите адрес электронной почты" required>
                        </div>
                        <div class="form-group">
                            <label for="userName">Имя пользователя</label>
                            <input required name="userName" type="text" class="form-control" id="userName"
                                placeholder="Введите имя пользователя...">
                        </div>
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea required name="description" type="text" class="form-control" id="description"
                                placeholder="Введите описание..."></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                        <button type="Submit" class="btn btn-primary">Добавить!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if ($isAdmin): ?>
    <form action="adminLogout" method="POST">
        <button type="Submit" class="btn btn-danger m-3">
            Выход из режима админимтратора
        </button>
    </form>
    <?php else: ?>
    <button type="button" class="btn btn-danger m-3" data-bs-toggle="modal" data-bs-target="#adminModal">
        Вход администротора
    </button>
    <div class="modal fade" id="adminModal" tabindex="-1" aria-labelledby="adminModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h5 class="modal-title" id="adminModalLabel">Вход администратора</h5>
                </div>
                <form action="adminLogin" method="POST">
                    <div class="m-3">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Логин</label>
                            <input name="log" type="text" class="form-control" id="exampleFormControlInput1"
                                placeholder="login" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Пароль</label>
                            <input name="pas" type="password" class="form-control" id="exampleFormControlInput1"
                                placeholder="password..." required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                        <button type="Submit" class="btn btn-primary">Вход</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif; ?>
</body>