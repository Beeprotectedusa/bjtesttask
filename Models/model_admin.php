<?php

Class Model_Admin Extends Model_Base {
    public function setAdmin($hash) {
        $this->connectToDb();
        $sql = "SELECT * FROM `admin`";
        $res_data = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($res_data) == 0) {
            $sql = "INSERT INTO `admin` (`hash`) VALUES ('$hash');";
            mysqli_query($this->db, $sql);
        } else {
            $admin = mysqli_fetch_array($res_data);
            $adminHash = $admin['hash'];
            $sql = "UPDATE `admin` SET `hash` = '$hash' WHERE `admin`.`hash` = '$adminHash';";
            mysqli_query($this->db, $sql);
        }
        $this->disConnectDb();
    }

    public function isAdmin($hash) {
        $this->connectToDb();
        $sql = "SELECT * FROM `admin`";
        $res_data = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($res_data) == 0) {
            $this->disConnectDb();
            return false;
        } else {
            $admin = mysqli_fetch_array($res_data);
            $this->disConnectDb();
            $adminHash = $admin['hash'];
            if ($adminHash == $hash) 
                return true;
            else 
                return false;
        }
    }
}

?>