<?php

Class Model_Task Extends Model_Base {
    public function getPaginationCount() {
        $this->connectToDb();
        $count_sql = "SELECT COUNT(*) FROM `tasks`";
        $result = mysqli_query($this->db, $count_sql);
        $this->disConnectDb();
        return mysqli_fetch_array($result)[0];
    }

    public function getPaginationData($offset, $size_page, $category , $order) {
        $this->connectToDb();
        $res = array();
        $arr1 = array('email', 'userName', 'complited');
        $arr2 = array('DESC','ASC');
        $catNumber = (int) $category;
        $orderNumber = (int) $order;
        if ($arr1[$catNumber] && $arr2[$orderNumber]) {
            $sql = "SELECT * FROM `tasks` ORDER BY `$arr1[$catNumber]` ". $arr2[$orderNumber] ." LIMIT $offset, $size_page";
        } else {
            $sql = "SELECT * FROM `tasks` LIMIT $offset, $size_page";
        }
        $res_data = mysqli_query($this->db, $sql);
        while($row = mysqli_fetch_array($res_data)){
            array_push($res, $row);
        }
        $this->disConnectDb();
        return $res;
    }

    function shieldPostInput($str) {
        return htmlentities(mysqli_real_escape_string($this->db,$str));
    }

    public function addTask($data) {
        if(isset($data['email']) && isset($data['userName']) && isset($data['description'])){
            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $this->connectToDb();
                $email = $this->shieldPostInput($data['email']);
                $userName = $this->shieldPostInput($data['userName']);
                $description = $this->shieldPostInput($data['description']);
                $sql = "INSERT INTO `tasks` (`id`, `email`, `userName`, `description`, `complited`, `edited`) VALUES (NULL, '$email', '$userName', '$description', '0', '0');";
                mysqli_query($this->db, $sql);
                $this->disConnectDb();
            }
        }
    }

    public function hasTask($id) {
        $this->connectToDb();
        $id = (int) $id;
        $sql = "SELECT COUNT(*) FROM `tasks` WHERE id=$id";
        $res_data = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($res_data) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function deleteTask($id) {
        $this->connectToDb();
        $id = (int) $id;
        $sql ="DELETE FROM `tasks` WHERE `id` = '$id'";
        mysqli_query($this->db, $sql);
        $this->disConnectDb();
    }

    public function compliteTask($id) {
        $this->connectToDb();
        $id = (int) $id;
        $sql ="UPDATE `tasks` SET `complited` = '1' WHERE `id` = '$id'";
        mysqli_query($this->db, $sql);
        $this->disConnectDb();
    }

    public function editTask($id,$data) {
        if(isset($data['email']) && isset($data['userName']) && isset($data['description'])){
            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $this->connectToDb();
                $id = (int) $id;
                $email = $this->shieldPostInput($data['email']);
                $userName = $this->shieldPostInput($data['userName']);
                $description = $this->shieldPostInput($data['description']);
                $sql ="UPDATE `tasks` SET
                `email` = '$email', 
                `userName` = '$userName',
                `description` = '$description',
                `edited` = '1' WHERE `tasks`.`id` = '$id'";
                mysqli_query($this->db, $sql);
                $this->disConnectDb();
            }
        }
    }

}

?>