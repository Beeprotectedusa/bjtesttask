<?php

spl_autoload_register(function ($class_name) {

    $fileName = strtolower($class_name) . '.php';

    $file = sitePath . 'Models' . DIRSEP . $fileName;
    
    if (file_exists($file) == false) {

            return false;

    }


    include ($file);
});
