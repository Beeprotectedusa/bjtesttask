<?php


Class Controller_complited Extends Controller_Base {
    function deleteTask(){
        $model = new Model_Task($this->registry);
        $id = $_GET['id'];
        if ($model->hasTask($id)) {
            $model->compliteTask($id);
        }
    }

    function index() {
        $admin = new Model_Admin($this->registry);
        if ($admin->isAdmin($_COOKIE['admin'])) {
            $this->deleteTask();
            setcookie("success", true, time()+3600);
        } else {
            setcookie("error", true, time()+3600);
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}


?>