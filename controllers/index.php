<?php


Class Controller_index Extends Controller_Base {
    private int $pageno;
    function checkCookie($name) {
        if(isset($_COOKIE["$name"])) {
            $this->registry['template']->set("$name",  true);
            setcookie("$name", "", time() - 3600);
        } else {
            $this->registry['template']->set("$name",  false);
        }
    }

    function paginationCreate($category, $order) {
        $size_page = 3;
        $offset = ($this->pageno-1) * $size_page;
        $model = new Model_Task($this->registry);
        $total_rows = $model->getPaginationCount($offset, $size_page);
        $total_pages = ceil($total_rows / $size_page);
        if ($this->pageno >= $total_pages) {
            $this->pageno = $total_pages;
            $offset = ($this->pageno-1) * $size_page;
            $total_rows = $model->getPaginationCount($offset, $size_page);
            $total_pages = ceil($total_rows / $size_page);
        }
        $this->registry['template']->set('total_pages',  $total_pages);
        $this->registry['template']->set('pageno',  $this->pageno);
        $this->registry['template']->set('offset',  $offset);
        $this->registry['template']->set('data', $model->getPaginationData($offset, $size_page, $category, $order));
    }
    function isAdmin() {
        if (isset($_COOKIE['admin'])) {
            $model = new Model_Admin($this->registry);
            return $model->isAdmin($_COOKIE['admin']);
        } else {
            return false;
        }
    }

    function index() {
        $this->registry['template']->set('isAdmin',  $this->isAdmin());
        if (isset($_GET['pageno'])) {
            $this->pageno = $_GET['pageno'];
        } else {
            $this->pageno = 1;
        }
        $category = 0;
        $order = 0;
        if (isset($_COOKIE['category']) && isset($_COOKIE['order'])) {
            $category = $_COOKIE['category'];
            $order = $_COOKIE['order'];
        }
        $this->registry['template']->set('category',  $category);
        $this->registry['template']->set('order',  $order);
        $this->paginationCreate($category, $order);
        $this->checkCookie("success");
        $this->checkCookie("adminLogError");
        $this->checkCookie("adminLogSuccess");
        $this->checkCookie("error");

        $this->registry['template']->show('index');
    }
}


?>