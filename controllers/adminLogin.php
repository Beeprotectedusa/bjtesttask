<?php


Class Controller_adminLogin Extends Controller_Base {
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function index() {
        if ($_POST['log'] == 'admin' && $_POST['pas'] == '123'){
            $model = new Model_Admin($this->registry);
            $hash = hash('ripemd160',$this->generateRandomString());
            setcookie("admin", $hash, time()+3600);
            $model->setAdmin($hash);
            setcookie("adminLogSuccess", true, time()+3600);
        } else {
            setcookie("adminLogError", true, time()+3600);
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}


?>