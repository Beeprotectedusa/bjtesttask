<?php


Class Controller_edit Extends Controller_Base {
    function index() {
        $model = new Model_Task($this->registry);
        $admin = new Model_Admin($this->registry);
        if ($admin->isAdmin($_COOKIE['admin'])) {
            $model->editTask($_GET['id'], $_POST);
            setcookie("success", true, time()+3600);
        } else {
            setcookie("error", true, time()+3600);
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}


?>