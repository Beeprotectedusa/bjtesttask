<?php


Class Controller_add Extends Controller_Base {
    function index() {
        $model = new Model_Task($this->registry);
        $model->addTask($_POST);
        setcookie("success", true, time()+3600);
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}


?>