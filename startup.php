<?php

spl_autoload_register(function ($class_name) {

    $fileName = strtolower($class_name) . '.php';

    $file = sitePath . 'Classes' . DIRSEP . $fileName;

    if (file_exists($file) == false) {

            return false;

    }


    include ($file);
});


$registry = new Registry;